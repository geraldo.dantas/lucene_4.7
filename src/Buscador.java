/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author José Geraldo
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

public class Buscador {
    private static Logger logger = Logger.getLogger(Buscador.class);
    
    private final String diretorioDoIndice = "C:\\GitLab\\lucene_4.7\\src\\Indice";
 //   private final String diretorioDoIndice = System.getProperty("user.home") + "\\Documents\\NetBeansProjects\\lucene_4.7\\src\\Indice"; 
    
    public ArrayList<String> modelo_Vetorial(String palavra_chave) {
        ArrayList<String> listaInformativa = new ArrayList<String>();
        try {
            Directory diretorio = new SimpleFSDirectory(new File(this.diretorioDoIndice));
            IndexReader leitor = DirectoryReader.open(diretorio);
            IndexSearcher buscador = new IndexSearcher(leitor);
            buscador.setSimilarity(new DefaultSimilarity());
            
            Analyzer analisador = new StandardAnalyzer(Version.LUCENE_47);
            QueryParser parser = new QueryParser(Version.LUCENE_47, "Texto", analisador);
            Query consulta = parser.parse(palavra_chave);
            
            long inicio = System.currentTimeMillis();    
            TopDocs resultado = buscador.search(consulta, 100);
            long fim = System.currentTimeMillis();
            int totalDeOcorrencias = resultado.totalHits;
                        
            for (ScoreDoc sd : resultado.scoreDocs) {
                Document documento = buscador.doc(sd.doc);
                String docNome = documento.get("Caminho");                
                String doc[] = docNome.split("Biblioteca");
                int pos = doc.length;
                                            
                listaInformativa.add("Documento: " + doc[pos-1].substring(1) + " Score: " + sd.score);                   
            }
                        
            listaInformativa.add("\n\nTotal de documentos encontrados:" + totalDeOcorrencias);
            listaInformativa.add("Tempo total para busca: " + (fim - inicio) + "ms \n");
            leitor.close();
        } catch (Exception e) {
            listaInformativa.add(e.toString());
        }
        return listaInformativa;
    }
    
    public ArrayList<String> modelo_BM25(String palavra_chave) throws IOException, ParseException{
        ArrayList<String> listaInformativa = new ArrayList<String>();   
      
        Directory diretorio = new SimpleFSDirectory(new File(this.diretorioDoIndice));
        IndexReader leitor = DirectoryReader.open(diretorio);        
        IndexSearcher buscador = new IndexSearcher(leitor);
        buscador.setSimilarity(new BM25Similarity());
        
        long inicio = System.currentTimeMillis();
        Analyzer analisador = new StandardAnalyzer(Version.LUCENE_47);
        QueryParser parser = new QueryParser(Version.LUCENE_47, "Texto", analisador);        
        Query consulta = parser.parse(palavra_chave);
        long fim = System.currentTimeMillis();
               
        for (ScoreDoc resultado : buscador.search(consulta, Integer.MAX_VALUE).scoreDocs) {
            Document documento = buscador.doc(resultado.doc);
            String docNome = documento.get("Caminho");                
            String doc[] = docNome.split("Biblioteca");
            int pos = doc.length;
                       
            listaInformativa.add("Documento: " + doc[pos-1].substring(1) + " Score: " + resultado.score); 
        }
                    
        int totalDeOcorrencias = buscador.search(consulta, Integer.MAX_VALUE).totalHits;
        listaInformativa.add("\n\nTotal de documentos encontrados:" + totalDeOcorrencias);
        listaInformativa.add("Tempo total para busca: " + (fim - inicio) + "ms");
               
        return listaInformativa;
    } 
    
}