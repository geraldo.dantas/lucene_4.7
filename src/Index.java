
import java.io.File;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author José Geraldo
 */
public class Index {
    
    Integer id;
    String nome;
    String tipo;
    String tamanho;
    String diretorio; 
    String[] extensoes;

    public Index() {
        this.id = 0;
        this.nome = "";
        this.tipo = "";
        this.tamanho = "";
        this.diretorio = "C:\\GitLab\\lucene_4.7\\src\\Indice";
     //     this.diretorio = System.getProperty("user.home") + "\\Documents\\NetBeansProjects\\lucene_4.7\\src\\Indice";
        this.extensoes = new String("cfe,cfs,si,gen,txt").split(",");
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getDiretorio() {
        return diretorio;
    }

    public void setDiretorio(String diretorio) {
        this.diretorio = diretorio;
    }
    
    // verifica se a pasta Indice contém arquivos de indexação e retorna os arquivos para o painel informativo de indices
    public ArrayList<Index> localizaIndices() throws Exception{
        ArrayList<Index> listaIndex = new ArrayList<Index>();
        
        int id=1; 
        boolean contem = false;
        File caminho = new File(this.diretorio);
        boolean caminhoValido = caminho.exists();
        boolean eDiretorio = caminho.isDirectory();
        
        if(caminhoValido && eDiretorio){
            String[] conteudo = caminho.list();

            // percorre todo o conteúdo da pasta
            for (String arquivo : conteudo) {
                contem = false;
                Index index = new Index();
                String extensao = "";
                
                //verifica se a pasta contem algum arquivo qualquer e coleta a extenção;
                for(int a=0; a<= (this.extensoes.length -1); a++ ){   
                    if(arquivo.contains(this.extensoes[a])){
                        extensao = this.extensoes[a];
                        contem = true;   
                        break;
                    }        
                }

                //verifica se a pasta contem algum arquivo e coleta informações;
                if (contem){   
                    index.id = id++;
                    index.diretorio = this.diretorio;
                    index.nome = arquivo.replace(extensao, "");
                    index.tipo = extensao;

                    java.io.File arq = new java.io.File(this.diretorio+"\\"+index.nome+extensao); 
                    index.tamanho = Long.toString((arq.length() / 1024)+1)+" KB";

                    listaIndex.add(index);
               }                            
            }
        }        
                
        return listaIndex;        
    }
    
    public void apagaIndices(String pasta) {
                
        File diretorio = new File(pasta);
        if (diretorio.exists()) {
            File arquivos[] = diretorio.listFiles();
            for (File arquivo : arquivos) {
                arquivo.delete();
            }
        }
    }
    
}
