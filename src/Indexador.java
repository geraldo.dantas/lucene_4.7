/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author José Geraldo
 */

        


import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.apache.tika.Tika;

public class Indexador {  
    private static Logger logger = Logger.getLogger(Indexador.class);

    public final String diretorio_src_dos_indices = "C:\\GitLab\\lucene_4.7\\src\\Indice";
    public final String diretorio_teste_dos_indices = "C:\\GitLab\\lucene_4.7\\src\\Teste";
    public final String diretorio_build_dos_indices = "C:\\GitLab\\lucene_4.7\\build\\classes\\Indice";  
   /*
    public final String diretorio_src_dos_indices = System.getProperty("user.home") + "\\Documents\\NetBeansProjects\\lucene_4.7\\src\\Indice";
    public final String diretorio_teste_dos_indices = System.getProperty("user.home") + "\\Documents\\NetBeansProjects\\lucene_4.7\\src\\Teste";
    public final String diretorio_build_dos_indices = System.getProperty("user.home") + "\\Documents\\NetBeansProjects\\lucene_4.7\\build\\classes\\Indice"; 
    */ 
    private final String diretorioParaIndexar = "C:\\GitLab\\lucene_4.7\\src\\Biblioteca";    
    private IndexWriter writer;   
    private Tika tika;
   
    public ArrayList<String> indexaArquivosDoDiretorio() {
        ArrayList<String> listaInformativa = new ArrayList<String>();
        
        try {
            File diretorio = new File(diretorio_src_dos_indices);
            // {5}
            Directory d = new SimpleFSDirectory(diretorio);
            listaInformativa.add("Diretorio do indice: " + diretorio_src_dos_indices);
            // {6}
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_47);
            // {7}
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_47,analyzer);
            // {8}
            writer = new IndexWriter(d, config);            
            long inicio = System.currentTimeMillis();
            listaInformativa = indexaArquivosDoDiretorio(new File(diretorioParaIndexar));
            // {12}
            writer.commit();
            writer.close();
            long fim = System.currentTimeMillis();
        //    listaInformativa.add(" \n \nTempo para indexar: " + ((fim - inicio) / 1000) + "s");
        } catch (IOException e) {
            listaInformativa.add(e.toString());
        }
        return listaInformativa;
    }

    // Apaga arquivos de indexação
    public void apagaIndices(File diretorio) {
        if (diretorio.exists()) {
            File arquivos[] = diretorio.listFiles();
            for (File arquivo : arquivos) {
                arquivo.delete();
            }
        }
    }

    public ArrayList<String> indexaArquivosDoDiretorio(File raiz) {
        
        ArrayList<String> informativo = new ArrayList<String>();
        Processador processa = new Processador();
        
        FilenameFilter filtro = new FilenameFilter() {
            // Lista de extensões de arquivo permitidos para indexação
            public boolean accept(File arquivo, String nome) {
                if (   nome.toLowerCase().endsWith(".pdf")
                    || nome.toLowerCase().endsWith(".odt")
                    || nome.toLowerCase().endsWith(".doc")
                    || nome.toLowerCase().endsWith(".docx")
                    || nome.toLowerCase().endsWith(".ppt")
                    || nome.toLowerCase().endsWith(".pptx")
                    || nome.toLowerCase().endsWith(".xls")
                    || nome.toLowerCase().endsWith(".txt")
                    || nome.toLowerCase().endsWith(".rtf")) {
                    return true;
                }
                return false;
            }
        };
                
        for (File arquivo : raiz.listFiles(filtro)) {   
                                
            if (arquivo.isFile()) {                
                
                StringBuffer msg = new StringBuffer();
                String textoExtraido = "";     
                String textoTAG = "";
                String textoProcessado = "";
                
                msg.append("Arquivo indexado: ");
                msg.append(arquivo.getAbsoluteFile());
                msg.append(", ");
                msg.append(arquivo.length() / 1000);
                msg.append("kb");
                informativo.add(msg.toString());
                
                try {
                    long inicio = System.currentTimeMillis();
                    
                    // Extrai o texto para ser indexado
                    textoExtraido = getTika().parseToString(arquivo);  
                    // Realiza o processo de separação por tags                    
                    textoTAG = processa.filtra_tags(textoExtraido);  
                    // Realiza o processo de remoção de simbolos, pontuação desnecessários e codificação de caracteres                  
                    textoProcessado = processa.pre_proecssamento(textoTAG);    
                    
                    long fim = System.currentTimeMillis();
                                       
                    //faz a indexação do texto processado
                    indexaArquivo(arquivo, textoProcessado);
                    
                //  imprime arquivos para teste    
                  imprimeTextoTeste(textoExtraido,"entrada",arquivo.getName());             
                  imprimeTextoTeste(textoTAG,"tag",arquivo.getName()); 
                  imprimeTextoTeste(textoProcessado,"filtro",arquivo.getName());
                //    informativo.add("\nTempo de Pré-Processamento do Documento : " + arquivo.getName()  + ((fim - inicio) / 1000) + "s");
                }
                catch (Exception e){ informativo.add(e.toString()); }
            } 
            else { indexaArquivosDoDiretorio(arquivo); }
                   
        }
        return informativo;
    }

    private void indexaArquivo(File arquivo, String textoExtraido) {
        SimpleDateFormat formatador = new SimpleDateFormat("yyyyMMdd");
        String ultimaModificacao = formatador.format(arquivo.lastModified());
        // {10}               
        Document documento = new Document();
        documento.add(new TextField("UltimaModificacao", ultimaModificacao, Store.YES));
        documento.add(new TextField("Caminho", arquivo.getAbsolutePath(), Store.YES));
        documento.add(new TextField("Texto", textoExtraido, Store.YES));
            
        try {
            // {11}
            getWriter().addDocument(documento);
        } catch (Exception e) {
            logger.error(e);
        }
    }
    
    // Imprime arquivos para testes
    public void imprimeTextoTeste(String texto, String tipo, String nome){

        FileWriter arquivo;
        String local = new String();

        switch(tipo){
            case "entrada":{
                local = this.diretorio_teste_dos_indices+"\\entrada_"+nome;
                break;
            }
            case "filtro":{                        
                local = this.diretorio_teste_dos_indices+"\\filtro_"+nome;
                break;
            }
            case "tag":{                        
                local = this.diretorio_teste_dos_indices+"\\tag_"+nome;
                break;
            }
            default:{ local = this.diretorio_teste_dos_indices+"\\erro.txt"; }
        }

        try {
                arquivo = new FileWriter(new File(local));
                arquivo.append(texto);
                arquivo.close();
        } 
        catch (IOException e) { e.printStackTrace(); } 
    }
            
    public Tika getTika() {
        if (tika == null) {
            tika = new Tika();
        }
    return tika;
    }

    public IndexWriter getWriter() {
        return writer;
    }
}