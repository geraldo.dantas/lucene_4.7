/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author José Geraldo
 */

import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;


import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

public class Processador{
        
    public static String pre_proecssamento(String textFile) throws Exception {
        // Instancia o StopWord com codificação no padrão americano 
        CharArraySet stopWords = EnglishAnalyzer.getDefaultStopSet();
        // Cria o token, converte para minúsculo e elimina espaços no início e fim do texto 
        TokenStream tokenStream = new StandardTokenizer(Version.LUCENE_47, new StringReader(textFile.trim().toLowerCase()));

        tokenStream = new ASCIIFoldingFilter(tokenStream);  //Trata a codificação de caracteres
        tokenStream = new StopFilter(Version.LUCENE_47, tokenStream, stopWords); // Elimina StopWords
        tokenStream = new StandardFilter(Version.LUCENE_47, tokenStream); // Elimina simbolos, pontuação desnecessários
        
        StringBuilder sb = new StringBuilder();
        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
            String term = charTermAttribute.toString();
            sb.append(term + " ");
        }
        return sb.toString();
    }

    public static String filtra_tags(String texto){
        
        // Relação de TAGs para serem procuradas e analisadas no filtro
        
        List<String> lastaTAG = new LinkedList<String>();
        lastaTAG.add("AU"); 
        lastaTAG.add("TI"); 
        lastaTAG.add("SO"); 
        lastaTAG.add("MJ"); 
        lastaTAG.add("MN"); 
        lastaTAG.add("AB"); 
        lastaTAG.add("EX"); 
        
        String documento = new String(""); // retorna o documento pós processamento
        String termo = new String(""); // controla os termos tokenizados
        String ultimaTAG = " "; // controla a ultima tag analisada
        String lista[]; // auxiliar para controlar os termos que serão analisados
        String tag_AU = "AU "; // recebe todos os termos específicos para a tag AU
        String tag_TI = "TI "; // recebe todos os termos específicos para a tag TI
        String tag_SO = "SO "; // recebe todos os termos específicos para a tag SO
        String tag_MJ = "MJ "; // recebe todos os termos específicos para a tag MJ
        String tag_MN = "MN "; // recebe todos os termos específicos para a tag MN
        String tag_AB = "AB "; // recebe todos os termos específicos para a tag AB
        String tag_EX = "EX "; // recebe todos os termos específicos para a tag EX
         
        
        try{
            // Quebra o texto do documento inteiro em token de termos separados por delimitador de espaço
            lista = texto.split("\n");
            String aux = " ";
            
            for (int a=0; a<lista.length; a++){
                aux += lista[a] + " ";
            }
            lista = aux.split(" ");
            
            for (int a=0; a<lista.length; a++){
                System.out.println(lista[a]);
            }
            // Percorre todosos itens da lista para analisar cada termos separadamente 
            for (int a=0; a<lista.length; a++){
                
                //Analisa se o token é uma TAG ou um termo
                if(lastaTAG.contains(lista[a])){
                    // Se for TAG, configura a TAG que está sendo analisada no momento até surgir uma nova tag
                    ultimaTAG = lista[a];
                    termo = "";
                }    
                // Se for um termo, prepara o termo para ser atribuído à TAG correta
                else{ termo = lista[a]; }

                // Analisa qual será a TAG de destino para receber o termo
                switch(ultimaTAG){ 
                    case "AU": { tag_AU += " " + termo; break; } 
                    case "TI": { tag_TI += " " + termo; break; }
                    case "SO": { tag_SO += " " + termo; break; }
                    case "MJ": { tag_MJ += " " + termo; break; }
                    case "MN": { tag_MN += " " + termo; break; }
                    case "AB": { tag_AB += " " + termo; break; }
                    case "EX": { tag_EX += " " + termo; break; }
                    default:  { continue; }
                } 
            }
        }catch(Exception erro){
            documento = "Erro ao processar o documento \n" + erro.getMessage();
            System.out.println(documento);
        }
        // Mescla todas as TAG com seus respectivos termos em campo único
        documento = tag_AU +" "+ tag_TI +" "+  tag_SO +" "+  tag_MJ +" "+  tag_MN +" "+  tag_AB +" "+  tag_EX;
        return documento;    
    }
}